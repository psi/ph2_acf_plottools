# Some useful scripts

## pyUtils

Here are some useful python scripts, mostly for plotting. Most of them have meaningful comments. 

- `getcanvas.py`: get the canvas from a root file and perform some operations on it (e.g. draw and save the canvas, get the dictionary of the canvas, etc.).
- `saveCanvas.py`: save the canvases as png files. Type `python saveCanvas.py -h` for help.
- `delta_threshold_noise.py`: plot the differences of the threshold and noise distribution between two Runs. Type `python delta_threshold_noise.py -h` for help.