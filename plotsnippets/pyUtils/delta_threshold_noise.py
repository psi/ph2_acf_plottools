#!/usr/bin/python
import os
import argparse
import math
import difflib
import ROOT
from getcanvas import getCanvas_dict, extract_run_number, vcal2charge


def delta_distribution(hist1 : ROOT.TH2, hist2 : ROOT.TH2) -> ROOT.TH2:
    # Calculate the difference between the two histograms
    delta_map= hist1.Clone()
    delta_map.Add(hist2, -1)

    # Set the z-axis range as the maximum to the minimum of the delta histogram
    delta_map.GetZaxis().SetRangeUser(delta_map.GetMinimum()-1,delta_map.GetMaximum()+1)

    return delta_map


def get_abs_delta_map(delta_map : ROOT.TH2) -> ROOT.TH2:
    # Set the bin value as its absolute value
    abs_delta_map = delta_map.Clone()
    for i in range(1, abs_delta_map.GetNbinsX() + 1):
        for j in range(1, abs_delta_map.GetNbinsY() + 1):
            abs_delta_map.SetBinContent(i, j, math.fabs(delta_map.GetBinContent(i, j)))
    
    # Get the histogram title and change it
    hist_title = abs_delta_map.GetTitle()
    hist_title = hist_title.replace('(', '').replace(')', '')
    hist_title = hist_title.replace('Noise Map', 'Abs(#DeltaNoise) Map')
    hist_title = hist_title.replace('Threshold Map', 'Abs(#DeltaThreshold) Map')
    
    abs_delta_map.SetTitle(hist_title)
    return abs_delta_map


def draw_abs_delta_distribution(delta_map : ROOT.TH2, canvas_name : str, run1 : str, run2 : str, outdir : str):
    # Draw the difference between two histograms
    abs_delta_canvas = ROOT.TCanvas(canvas_name, delta_map.GetTitle(), 1920, 1080)
    abs_delta_canvas.cd()

    # Get the statistics box and change its position and draw it
    ROOT.gStyle.SetOptStat()
    stats_box = delta_map.GetListOfFunctions().FindObject("stats")
    stats_box.SetX1NDC(0.)
    stats_box.SetX2NDC(0.14)
    stats_box.SetY1NDC(0.905)
    stats_box.SetY2NDC(1)
   
    abs_delta_map = get_abs_delta_map(delta_map)
    abs_delta_map.Draw("colz")
    abs_delta_canvas.SetLogz()
    abs_delta_canvas.Update()
    abs_delta_canvas.SaveAs(f"{outdir}/absdelta_R{run1}_R{run2}_{canvas_name}.png")


def get_thres_noise_delta(thres_delta_map : ROOT.TH2, noise_delta_map : ROOT.TH2, chip : str) -> ROOT.TH2:
    thres_delta_M = round(thres_delta_map.GetMaximum())
    thres_delta_m = round(thres_delta_map.GetMinimum()) - 1
    noise_delta_M = round(noise_delta_map.GetMaximum())
    noise_delta_m = round(noise_delta_map.GetMinimum()) - 1

    # Fill a histogram with the delta threshold and noise values
    th_no_delta_hist = ROOT.TH2D(f"delta_threshold_noise_{chip}", "#DeltaThreshold v.s. #DeltaNoise ", 
                                (thres_delta_M - thres_delta_m)*1, thres_delta_m, thres_delta_M, 
                                (noise_delta_M - noise_delta_m)*1, noise_delta_m, noise_delta_M)                 

    th_no_delta_hist.GetXaxis().SetTitle("#DeltaThreshold(#DeltaVCAL)")
    th_no_delta_hist.GetYaxis().SetTitle("#DeltaNoise(#DeltaVCAL)")

    for i in range(1, thres_delta_map.GetNbinsX() + 1):
        for j in range(1, thres_delta_map.GetNbinsY() + 1):
            th_no_delta_hist.Fill(thres_delta_map.GetBinContent(i, j), noise_delta_map.GetBinContent(i, j))
    
    return th_no_delta_hist


def draw_delta_thres_noise(delta_thno_hist : ROOT.TH2, canvas_name : str, run1 : str, run2 : str, outdir : str):
    delta_thno_hist.SetTitle(f"{canvas_name} {delta_thno_hist.GetTitle()}")
    # Draw the difference between two histograms
    delta_canvas = ROOT.TCanvas(canvas_name, canvas_name, 1920, 1080)
    delta_canvas.cd() 
    
    ROOT.gStyle.SetOptStat()
    rebin_delta_thno_hist = delta_thno_hist.Clone()
    rebin_delta_thno_hist.Rebin2D(2, 2)
    rebin_delta_thno_hist.Draw("colz")
    delta_canvas.SetLogz()
    delta_canvas.Update()
    
    # Get the statistics box and change its position and draw it
    stats_box = rebin_delta_thno_hist.FindObject("stats")
    stats_box.SetX1NDC(0.)
    stats_box.SetX2NDC(0.15)
    stats_box.SetY1NDC(0.91)
    stats_box.SetY2NDC(1)
    
    delta_canvas.Update()
    delta_canvas.SaveAs(f"{outdir}/deltathno_R{run1}_R{run2}_{canvas_name}.png")

    # Zoom to the cluster of points
    mean_delta_thres = delta_thno_hist.GetMean(1)
    mean_delta_noise = delta_thno_hist.GetMean(2)
    stdev_delta_thres = delta_thno_hist.GetStdDev(1)
    stdev_delta_noise = delta_thno_hist.GetStdDev(2)
    zoom_delta_thno_hist = delta_thno_hist.Clone()
    zoom_delta_thno_hist.GetXaxis().SetRangeUser(mean_delta_thres - 8*stdev_delta_thres, mean_delta_thres + 8*stdev_delta_thres)
    zoom_delta_thno_hist.GetYaxis().SetRangeUser(mean_delta_noise - 8*stdev_delta_noise, mean_delta_noise + 8*stdev_delta_noise)
    zoom_delta_thno_hist.SetTitle(f"{delta_thno_hist.GetTitle()}, Zoomed")
    
    # Draw an extra canvas zooming to the cluster of points
    delta_canvas_zoom = ROOT.TCanvas(f"{canvas_name}_zoom", f"{canvas_name}_zoom", 1920, 1080)
    delta_canvas_zoom.cd()
    zoom_delta_thno_hist.Draw("colz")
    delta_canvas_zoom.SetLogz()
    delta_canvas_zoom.Update()
    
    # Get the statistics box and change its position and draw it
    stats_box = zoom_delta_thno_hist.FindObject("stats")
    stats_box.SetX1NDC(0.)
    stats_box.SetX2NDC(0.15)
    stats_box.SetY1NDC(0.91)
    stats_box.SetY2NDC(1)

    delta_canvas_zoom.SaveAs(f"{outdir}/deltathno_R{run1}_R{run2}_{canvas_name}_zoom.png")


def draw_compare_hist(hist1 : ROOT.TH1, hist2 : ROOT.TH1, canvas_name : str, run1 : str, run2 : str, outdir : str,
                      hist1_label : str, hist2_label: str, sel_cal_range : int):
    # Draw the two input histogram in the same canvas for comparison 
    compare_canvas = ROOT.TCanvas(canvas_name, canvas_name, 1920, 1080)
    compare_canvas.cd() 
    compare_canvas.GetPad(0).SetTopMargin(0.16)

    isNotNoise = 0 if "noise" in canvas_name.lower() else 1
    xunit   = 100 if isNotNoise == 1 else 10

    # Adjust the x axis range if needed
    xmax = max((((hist1.GetMean()*2)//xunit + 1)*xunit, ((hist2.GetMean()*2)//xunit + 1)*xunit,
                hist1.GetMean()+hist1.GetStdDev()*8,hist2.GetMean()+hist2.GetStdDev()*8))
    hist1.GetXaxis().SetRangeUser(0, xmax)
    hist2.GetXaxis().SetRangeUser(0, xmax)
    ymax = max(hist1.GetMaximum(), hist2.GetMaximum()) * 1.1
    hist1.GetYaxis().SetRangeUser(0, ymax)
    hist2.GetYaxis().SetRangeUser(0, ymax)
    
    hist1.Draw()    
    hist2.Draw("SAME")    
    hist1.SetLineColor(ROOT.kRed)
    hist2.SetLineColor(ROOT.kBlue)
    hist1.SetLineWidth(3)
    hist2.SetLineWidth(3)
    hist1.SetLineStyle(1)
    hist2.SetLineStyle(1)

    # Draw the legend
    legend = ROOT.TLegend(0.7, 0.64, 0.9, 0.84)
    legend.AddEntry(hist1, hist1_label, "l")
    legend.AddEntry(hist2, hist2_label, "l")
    legend.Draw()
    
    # Disable the statistics box
    hist1.SetStats(0) 
    hist2.SetStats(0) 

    topGaxis = ROOT.TGaxis(0, ymax, xmax, ymax, vcal2charge(0, sel_cal_range, isNotNoise), vcal2charge(xmax, sel_cal_range, isNotNoise), 510, "-")
    title = "Threshold " if isNotNoise == 1 else "Noise " 
    topGaxis.SetTitle(f"{title}(electrons)")
    topGaxis.SetTitleOffset(1.2)
    topGaxis.SetTitleSize(0.035)
    topGaxis.SetTitleFont(40)
    topGaxis.SetLabelFont(40)
    topGaxis.SetLabelSize(0.035)
    topGaxis.SetLabelOffset(0.001)
    topGaxis.SetLabelColor(ROOT.kRed)
    topGaxis.SetLineColor(ROOT.kRed)
    topGaxis.Draw()

    compare_canvas.Update()
    compare_canvas.SaveAs(f"{outdir}/compare_R{run1}_R{run2}_{canvas_name}.png")


def map_the_canvases(cname_set1 : set, cname_set2 : set) -> dict:
    # Map the canvases with the closest name in two set
    # Return a dictionary with the mapped canvas names 
    
    name_map = {}
    for cname1 in cname_set1:
        cname2 = difflib.get_close_matches(cname1, cname_set2, n=1)[0]
        name_map[cname1] = cname2
        cname_set2.remove(cname2)

    return name_map
    
def delta_thres_noise(run1_file_name : str, run2_file_name : str, outdir : str, label1 : str, label2 : str, force : bool = False):
    '''Compare the threshold and noise distributions of two runs, and save the difference'''

    canvas1_dict = getCanvas_dict(run1_file_name, False)
    canvas2_dict = getCanvas_dict(run2_file_name, False)
    # Check if the two runs have the same number of canvases
    if len(canvas1_dict) != len(canvas2_dict):
        print(f"Error: the two runs have different number of canvases: {len(canvas1_dict)} vs {len(canvas2_dict)}")
        return
    
    if force:
        name_map = map_the_canvases(set(canvas1_dict.keys()), set(canvas2_dict.keys())) 

    run1 = extract_run_number(run1_file_name)
    run2 = extract_run_number(run2_file_name)
    
    chip_title = "" 
    delta_thres_map, delta_noise_map = {}, {}
    for c1name in canvas1_dict.keys():
        if force:
            c2name = name_map[c1name]
        else:
            c2name = c1name
            if c2name not in canvas2_dict.keys():
                print(f"Error: canvas {c2name} is not found in the second run, use --force to force the comparison")
                return
        
        # Get the histograms from the input canvases
        hist1 = canvas1_dict[c1name].GetPrimitive(canvas1_dict[c1name].GetName())
        hist2 = canvas2_dict[c2name].GetPrimitive(canvas2_dict[c2name].GetName())
        
        if 'Threshold2D' in c1name or 'Noise2D' in c1name:
            chip_title = c1name.replace('Threshold2D_', '').replace('Noise2D_', '')
            delta_map = delta_distribution(hist1, hist2)
            draw_abs_delta_distribution(delta_map, c1name, run1, run2, outdir)

            if 'Threshold2D' in c1name:
                delta_thres_map[chip_title] = delta_map
            else:
                delta_noise_map[chip_title] = delta_map

        if 'Threshold1D' in c1name or 'Noise1D' in c1name:
            # Get the SEL_CAL_RANGE 
            charge_M = canvas1_dict[c1name].GetPrimitive("").GetWmax()
            vcal_M   = hist1.GetXaxis().GetXmax()
            sel_cal_range = 1 if charge_M > 7*vcal_M else 2

            draw_compare_hist(hist1, hist2, c1name, run1, run2, outdir, label1, label2, sel_cal_range)

    for chip in delta_thres_map.keys():
        delta_thno_hist = get_thres_noise_delta(delta_thres_map[chip], delta_noise_map[chip], chip)
        draw_delta_thres_noise(delta_thno_hist, chip, run1, run2, outdir)



if __name__ == "__main__":
    # Parse command-line arguments to get the input file names
    parser = argparse.ArgumentParser(description='''Compare the threshold and noise distributions of two runs, and save the difference. The two runs should have the same Optical group, Hybird ID, etc. Use --force to force the comparison.''')
    parser.add_argument('--run1', type=str, help='The name of the first run file')
    parser.add_argument('--run2', type=str, help='The name of the second run file')
    parser.add_argument('--label1', type=str, help='The label of the first run')
    parser.add_argument('--label2', type=str, help='The label of the second run')
    parser.add_argument('-o', '--outdir', type=str, default='delta_thres_noise', help='The name of the output directory')
    parser.add_argument('--force', action='store_true', help='Force to make the comparison, regardless of the hybird id')
    parser.add_argument('-help', action='help', help='Show this help message and exit')
    args = parser.parse_args()

    if not os.path.exists(args.outdir):
        os.makedirs(args.outdir)
        print(f"Directory '{args.outdir}' created successfully.")

    if args.label1 is None:
        args.label1 = extract_run_number(args.run1)
    if args.label2 is None:
        args.label2 = extract_run_number(args.run2)    

    delta_thres_noise(args.run1, args.run2, args.outdir, args.label1, args.label2, args.force)