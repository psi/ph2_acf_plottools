#!/usr/bin/python
import argparse
import json
import os

import ROOT
ROOT.gStyle.SetPalette(ROOT.kRainBow)


def double_gaus(x, p):
    '''Double gaussian function sharing the same mean value'''
    return p[1]*ROOT.TMath.Gaus(x[0], p[0], p[2]) + p[3]*ROOT.TMath.Gaus(x[0], p[0], p[4])


def rootfile_name(run_number : int, root_file_dir : str ="Results") -> str:
    return os.path.join(root_file_dir, "Run%06d_SCurve.root"%run_number)


def fit_threshold(canvas : ROOT.TCanvas):
    # Set the canvas as the current pad
    canvas.cd()
    hist = canvas.GetPrimitive(canvas.GetName())
    fx_xmin = max(hist.GetMean() - hist.GetStdDev()*4, hist.GetXaxis().GetXmin())
    fx_xmax = min(hist.GetMean() + hist.GetStdDev()*4, hist.GetXaxis().GetXmax())
    func = ROOT.TF1("DoubleGaus", double_gaus, fx_xmin, fx_xmax, 5)
    func.SetParameters(hist.GetMean(), hist.GetMaximum(), hist.GetStdDev()/2,
                       hist.GetMaximum()/20, hist.GetStdDev())
    hist.Fit(func, "0QR")  
    # "Q" option for fitting in the quiet mode
    # "R" option specifies a range fit
    func.SetLineColor(ROOT.kRed)

    A1 = func.GetParameter(1) 
    A2 = func.GetParameter(3)
    sigma = func.GetParameter(2) if A1 > A2 else func.GetParameter(4)
    return func.GetParameter(0), sigma


def get_threshold(root_file_name : str) -> dict:
    ROOT.gROOT.SetBatch(True)
    assert "SCurve" in root_file_name
    # Open the ROOT file
    root_file = ROOT.TFile.Open(root_file_name)
    if not root_file or root_file.IsZombie():
        print(f"Failed to open ROOT file: {root_file_name}")
        return

    # Recursive function to process subfolders
    def process_subfolder(subfolder):
        folder_dict = {}
        # Loop through objects in the subfolder
        for key in subfolder.GetListOfKeys():
            obj = key.ReadObj()

            # Check if the object is a canvas
            if isinstance(obj, ROOT.TCanvas) and "Threshold1D" in obj.GetName():
                canvas_name = obj.GetName()
                chipNum = canvas_name[canvas_name.find("Chip"):]
                folder_dict[chipNum] = fit_threshold(obj)

            elif isinstance(obj, ROOT.TDirectory):
                subsubfolder_name = obj.GetName()

                # Recursively process the subfolder
                folder_dict[subsubfolder_name] = process_subfolder(obj)
        return folder_dict
                

    # Start processing from the root directory
    thresholds = process_subfolder(root_file)

    # Clean up
    root_file.Close()

    return thresholds


def get_threshold_for_finedelay(finedelay : int, input_dir : str):
    run_number = finedelay_Run[finedelay]
    root_file_name = rootfile_name(run_number, input_dir)
    return get_threshold(root_file_name)


def process_all_delay(finedelay_Run : dict, input_dir : str) -> dict:
    # Loop through root files with all fine delay in the input directory
    delay_thr = {}
    for delay in finedelay_Run.keys():
        delay_thr[delay] = get_threshold_for_finedelay(delay, input_dir)
    return delay_thr



# Parse command-line arguments
parser = argparse.ArgumentParser(description='Fit threshold for fine delay')
parser.add_argument('-d', '--input_dir', type=str, default='Results', help='Input directory of ROOT file')
parser.add_argument('-o', '--output_dir', type=str, default='Results', help='Output directory for results')
parser.add_argument('-g', '--good', type=str,  help='The ROOT file with the best fine delay')
parser.add_argument('-b', '--begin', type=int, default=0,  help='Start value for fine delay')
parser.add_argument('-e', '--end',   type=int, default=5, help='Stop value for fine delay')
parser.add_argument('-r', '--run',   type=int, default=200, help='Run number of the first ROOT file')
parser.add_argument('-a', '--asyn', action='store_true', default=False, help='Run in asynchronous mode')
parser.add_argument('-help', action='help', help='Show this help message and exit')

args = parser.parse_args()

finedelay_Run = {i:args.run+i for i in range(args.begin, args.end)}
result = process_all_delay(finedelay_Run, args.input_dir)

if args.asyn:
    mode = "asy_"
else:
    mode = ""

# Save the result to a JSON file
with open(f'{arg.output_dir}/threshold_oscillation_%sresult.json'%mode, 'w') as fp:
    json.dump(result, fp)

if args.good is not None:
    with open(f'{arg.output_dir}/threshold_bestdelay_%sresult.json'%mode, 'w') as fp:
        json.dump(get_threshold(args.good), fp)
