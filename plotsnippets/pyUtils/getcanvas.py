#!/usr/bin/python
import os
import ROOT

def extract_run_number(file_name : str) -> str:
    # Extract the base name from the file path
    base_name = os.path.basename(file_name)

    # Split the base name into parts using "_" as the delimiter
    parts = base_name.split("_")

    # Extract the run number from the second part
    run_number = parts[0][3:]

    return run_number


def process_subfolder(subfolder, folder_structure, canvas_func : callable = None, **kwargs):
    ''' Recursively process the subfolders of the given subfolder. 
        Return the dictionary of TCanvas objects optionally respecting the directory structure of the input file.
        Can optionally apply the canvas_func to each TCanvas object.'''
    canvas_dict = {}
    for key in subfolder.GetListOfKeys():
        obj = key.ReadObj()

        # Check if the object is a canvas
        if isinstance(obj, ROOT.TCanvas):
            canvas_name = obj.GetName()
            canvas_dict[canvas_name.replace("(", "").replace(")", "")] = obj
            if canvas_func is not None:
                canvas_func(obj, **kwargs)

        # Check if the object is a subfolder
        elif isinstance(obj, ROOT.TDirectory):
            # Check if the subfolder is empty
            if obj.GetListOfKeys().GetSize() != 0:
                subsubfolder_name = obj.GetName()
                if folder_structure:
                    canvas_dict[subsubfolder_name] = process_subfolder(obj, folder_structure, canvas_func, **kwargs)
                else:
                    canvas_dict.update(process_subfolder(obj, folder_structure, canvas_func, **kwargs))
    return canvas_dict


def getCanvas_dict(input_file_name : str, folder_structure : bool = False) -> dict:
    ''' Process the input file recursively, read all the TCanvas objects from the input ROOT file,
        return the dictionary of TCanvas objects optionally respecting the directory structure of the input file.
        The typical result is a dictionary with the following structure 
        if folder_structure is True: 
        {"Detector": 
            {"Board_id":   
                { "OpticalGroup_id": 
                    {"Hybrid_id": 
                        {"Chip_id":
                            {"Canvas_name": TCanvas}
                        }}}}}
        else is: {"Canvas_name": TCanvas}
    '''
    ROOT.gROOT.SetBatch(True)
    with ROOT.TFile.Open(input_file_name) as input_file:
        if not input_file or input_file.IsZombie():
            print(f"Failed to open ROOT file: {input_file_name}")
            return
        
        # Start processing from the root directory
        canvas_dict = process_subfolder(input_file, folder_structure)

    return canvas_dict


def vcal2charge(vcal:float, cal_range=2, isNotNoise=1):
    return 40000/4096/cal_range*vcal + 64*isNotNoise


if __name__ == '__main__':
    import pprint
    pprint.pprint(getCanvas_dict("Results/Run000273_SCurve.root"))