#!/home/tepx/Packages/miniconda3/envs/plot/bin/python
import json

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import FuncFormatter, MultipleLocator
from scipy import optimize

palette   = {15:'red', 14:'blue', 13:'green', 12:'brown'}
palette_f = {15:'purple', 14:'cyan', 13:'olive', 12:'orange'}

def cosine_func(x, *param):
    return param[0] * np.cos(2*np.pi*(x-param[2])/(param[1])) + param[3]

def load_file(mode="", threoshld_oscil=None, thres_bestdelay=None):
    if threoshld_oscil is None:
        with open('Results/threshold_oscillation_%sresult.json'%mode, 'r') as fp:
            delay_thr_osci = json.load(fp)
    else:
        with open(threoshld_oscil, 'r') as fp:
            delay_thr_osci = json.load(fp)
    if thres_bestdelay is None:
        with open('Results/threshold_bestdelay_%sresult.json'%mode, 'r') as fp:
            thr_bestdelay = json.load(fp)
    else:
        with open(thres_bestdelay, 'r') as fp:
            thr_bestdelay = json.load(fp)
    return delay_thr_osci, thr_bestdelay

def cosine_format(params : list):
    sign = "-"
    if params[2] < 0:
        sign = ""    
    return r"$%.2f\times\cos{2\pi\,\frac{t%s%.2f}{%.2f}}+%.2f$"%(params[0], sign, params[2], abs(params[1]), params[3])

def vcal2charge(vcal:int, cal_range=2, isNotNoise=1):
    return 40000/4096/cal_range*vcal + 64*isNotNoise

def vcalcharge_convert(value:int, position) -> int:
    return round(vcal2charge(value))

def thres_oscil_plt(thresholds, thresholds_e, time, fit_x, fit_y, params, 
                    bestdelay, thr_bestdelay,
                    mode='', 
                    palette=palette, palette_f=palette_f):
    '''Do threshold oscillation plot with matplotlib'''
    for chip, thr in thresholds.items():
        fig, ax1 = plt.subplots(figsize=(9, 5))
        # Plot the thresholds v.s. fine delay 
        markers, caps, bars = ax1.errorbar(time, thr, yerr=thresholds_e[chip], fmt='o', 
                    elinewidth=1, markersize=4,
                    color=palette[chip])
        [bar.set_alpha(0.2) for bar in bars]

        ax1.plot(fit_x[chip], fit_y[chip], label=cosine_format(params[chip]),
                color=palette_f[chip])

        # Add arrow annotation pointing the best delay
        thr_best = thr_bestdelay[detec][board][optic][hybird]['Chip_%d'%chip]['Chip(%d)'%chip][0]
        ax1.scatter(bestdelay[chip], thr_best, color='black', marker='X',s=22,
                label='Best delay')
        
        vcal_min = np.min(thr-thresholds_e[chip])-2
        vcal_max = np.max(thr+thresholds_e[chip]*1.5) 
        ax1.set_ylim(vcal_min, vcal_max)
        
        # Set plot title and labels
        ax1.set_title('Threshold, Chip %d'%chip, fontsize=16)
        ax1.set_xlabel('Fine delay / 0.78ns', fontsize=15)
        ax1.set_ylabel('Threshold / $\Delta$VCAL', fontsize=15)

        # Automatically position the legend
        ax1.legend(loc='best', fontsize=14)

        # Add an axis for thresholds in electrons
        ax2 = ax1.twinx()
        ax2.set_ylim(vcal_min, vcal_max)
        ax2.yaxis.set_major_formatter(FuncFormatter(vcalcharge_convert))
        ax2.set_ylabel('Threshold / $e^-$', fontsize=15)
        
        # Save the plot
        plt.savefig('Results/thr_oscil_%schip%d.png'%(mode, chip), bbox_inches='tight', 
                    )
        #plt.show()
        plt.clf()

def read_threshold_and_dev(delay_thr_osci : dict, 
                           detec = "Detector", board = "Board_0", 
                           optic = "OpticalGroup_0", hybird= "Hybrid_1"):
    '''Read the threshold and deviation from the json file'''
    thresholds = {15:[], 14:[], 13:[], 12:[]}
    thresholds_e = {15:[], 14:[], 13:[], 12:[]}
    for delay, thr in delay_thr_osci.items():
        module = thr[detec][board][optic][hybird]
        for chipid in [15, 14, 13, 12]:
            if module.get('Chip_%d'%chipid) is not None:
                thresholds[chipid].append(module['Chip_%d'%chipid]['Chip(%d)'%chipid][0])
                thresholds_e[chipid].append(module['Chip_%d'%chipid]['Chip(%d)'%chipid][1])
    thresholds = {k:np.array(v) for k, v in thresholds.items() if len(v) > 0}
    thresholds_e = {k:np.array(v) for k, v in thresholds_e.items() if len(v) > 0}
    return thresholds, thresholds_e

def fit_result_for_module(thresholds, time, thresholds_e, period=32):
    fit_x, fit_y, params, param_var = {}, {}, {}, {} 

    for chip, thr in thresholds.items():
        # Fit the threshold with cosine function
        for phase in range(5):
            init_param = [np.max(thr)-np.min(thr), period, phase*period/5, np.mean(thr)]
            params[chip], param_var[chip] = optimize.curve_fit(
                                                cosine_func, time, thr, 
                                                p0 = init_param,
                                                sigma=thresholds_e[chip])
            if params[chip][0] > 0:
                break
    
        fit_x[chip] = np.linspace(time[0], time[-1], 1000)
        fit_y[chip] = cosine_func(fit_x[chip], *params[chip])

    return fit_x, fit_y, params, param_var

def main(bestdelay : dict, mode="", threoshld_oscil=None, thres_bestdelay=None,
         detec="Detector", board = "Board_0", optic="OpticalGroup_0", hybird= "Hybrid_1",
         use_matplotlib = True):
    delay_thr_osci, thr_bestdelay = load_file(mode, threoshld_oscil, thres_bestdelay)

    time = np.array(list(int(i) for i in delay_thr_osci.keys()))
    thresholds, thresholds_e = read_threshold_and_dev(delay_thr_osci, detec,
                                            board, optic, hybird)
    if mode == "":  # sync mode
        period = 32
    else:           # async mode
        period = 50
    fit_x, fit_y, params, param_var = fit_result_for_module(thresholds, time, thresholds_e, period)
    if use_matplotlib:
        thres_oscil_plt(thresholds, thresholds_e, time, fit_x, fit_y, params, bestdelay, 
                        thr_bestdelay, mode=mode, palette=palette, palette_f=palette_f)
       
detec = "Detector"
board = "Board_0"
optic = "OpticalGroup_0"
hybird= "Hybrid_1"
bestdelay = {15:17, 14:19, 13:19, 12:18}
use_matplotlib = True

#mode="asy_"
mode=""

main(bestdelay=bestdelay, mode=mode, 
     detec=detec, board=board, optic=optic, hybird=hybird,
     use_matplotlib=use_matplotlib)