#!/usr/bin/python
import os
import argparse
import math
import ROOT
from getcanvas import extract_run_number, process_subfolder

ROOT.gStyle.SetPalette(ROOT.kRainBow)

def double_gaus(x, p):
    '''Double gaussian function sharing the same mean value'''
    return p[1]*ROOT.TMath.Gaus(x[0], p[0], p[2]) + p[3]*ROOT.TMath.Gaus(x[0], p[0], p[4])

saving_list = {"PixelAlive", "SCurves", "Threshold", "Noise", "TDAC1D", "TDAC2D", "BERtest", "GenericDacDac"}
logz_list   = {"SCurves"}
style_list  = {"Threshold1D":{"xunit":100, "topGaxisOffset":-0.03,
                            "fit":double_gaus, "fit_func":"DoubleGaus"},
                            # "fit":'gaus', "fit_func":"Gaussian"},
               "Noise1D":{"xunit":10, "topGaxisOffset":-0.03, 
                          #"fit":'gaus'
                        },
               }


def position_gstat_panel(canvas : ROOT.TCanvas, canvas_name : str):
    # Set the canvas as the current pad
    canvas.cd()
    pad = ROOT.gPad

    stat_box = pad.GetPrimitive("stats")

    # Set the position of the stat box
    if "Threshold1D" in canvas_name or "Noise1D" in canvas_name:
        stat_box.SetX1NDC(0.6)
        stat_box.SetY1NDC(0.75)
        stat_box.SetX2NDC(0.90)
        stat_box.SetY2NDC(0.3)
    else:
        stat_box.SetX1NDC(0.0)
        stat_box.SetY1NDC(1.0)
        stat_box.SetX2NDC(0.12)
        stat_box.SetY2NDC(0.91)


def adjust_plot(canvas:ROOT.TCanvas, canvas_name:str, nofit:bool):
    # Set the canvas as the current pad
    canvas.cd()

    hist = canvas.GetPrimitive(canvas_name)

    if any(pattern in canvas_name for pattern in logz_list):
        canvas.SetLogz()

    if "SCurves" in canvas_name:
        yhist = hist.ProjectionY()
        ymax_bin = 0
        ymin_bin = yhist.GetNbinsX() + 1 
        
        # Find the minimum and maximum bin with non-zero content
        for ybin in range(round(yhist.GetNbinsX()/2.) - 1, yhist.GetNbinsX()+1):
            if yhist.GetBinContent(ybin) > 0:
                ymax_bin = ybin
        for ybin in range(round(yhist.GetNbinsX()/2.) + 1, 0, -1):
            if yhist.GetBinContent(ybin) > 0:
                ymin_bin = ybin
        
        ymax_bin = yhist.GetBinLowEdge(min(ymax_bin+5, yhist.GetNbinsX())) + yhist.GetBinWidth(ymax_bin)
        ymin_bin = yhist.GetBinLowEdge(max(ymin_bin-5, 1))
        hist.GetYaxis().SetRangeUser(ymin_bin, ymax_bin)
        
        topGaxis = canvas.GetPrimitive("")
        topGaxis.DrawAxis(hist.GetXaxis().GetXmin(), ymax_bin, hist.GetXaxis().GetXmax(), ymax_bin,
                          topGaxis.GetWmin(), topGaxis.GetWmax(), 510, "-")


    for name, param in style_list.items():
        if name in canvas_name:
            topGaxis = canvas.GetPrimitive("")
            
            if param.get('xunit') is not None:
            # Adjust the x axis range if needed
                xmax = max(((hist.GetMean()*3)//param['xunit'] + 1)*param['xunit'], hist.GetXaxis().GetXmax()/2)
                hist.GetXaxis().SetRangeUser(0, xmax)
                topGaxis.CenterTitle()

            if param.get('topGaxisOffset') is not None:
                topGaxis.SetLabelOffset(param['topGaxisOffset'])

            if param.get('fit') is not None and not nofit:
                fx_xmin, fx_xmax = hist.GetXaxis().GetXmin(), hist.GetXaxis().GetXmax()
                if type(param['fit']) is str and 'gaus' in param['fit']:
                    fx_xmin = max(hist.GetMean() - hist.GetStdDev()*2, fx_xmin)
                    fx_xmax = min(hist.GetMean() + hist.GetStdDev()*2, fx_xmax)
                    func = ROOT.TF1(param.get('fit_func', param['fit']), param['fit'], 
                                   fx_xmin, fx_xmax)
                elif param['fit_func'] == 'DoubleGaus':
                    fx_xmin = max(hist.GetMean() - hist.GetStdDev()*4, fx_xmin)
                    fx_xmax = min(hist.GetMean() + hist.GetStdDev()*4, fx_xmax)
                    func = ROOT.TF1(param['fit_func'], param['fit'], 
                                   fx_xmin, fx_xmax, 5)
                    func.SetParameters(hist.GetMean(), hist.GetMaximum(), hist.GetStdDev()/2,
                                       hist.GetMaximum()/20, hist.GetStdDev())
                    func.SetParNames("Fitted mean", "A1", "#sigma1", "A2", "#sigma2")

                hist.Fit(func, "0QR")  
                # "Q" option for fitting in the quiet mode
                # "R" option specifies a range fit
                func.SetLineColor(ROOT.kRed)
                func.Draw("SAME")
                ROOT.gStyle.SetOptFit()


def process_canvas(canvas : ROOT.TCanvas, output_folder : str, run_number : str = "000001", 
                   width : int = 1920, height : int = 1080, nofit : bool = False):
    canvas_name = canvas.GetName()
    if any(pattern in canvas_name for pattern in saving_list):
        canvas.cd()
        canvas.Draw("")
        canvas.SetCanvasSize(width, height)
        
        position_gstat_panel(canvas, canvas_name)
        adjust_plot(canvas, canvas_name, nofit)
        
        canvas.Modified()
        canvas.Update()

        plot_file_name = canvas_name.replace("(", "").replace(")", "")
        output_file_name = os.path.join(output_folder, f"Run{run_number}_{plot_file_name}.png")
        canvas.SaveAs(output_file_name)


def process_rootfile(root_file_name:str, output_folder:str, width:int, height:int, nofit:bool):
    ROOT.gROOT.SetBatch(True)
    
    # Extract the run number from the file name
    run_number = extract_run_number(root_file_name)
    
    with ROOT.TFile.Open(root_file_name) as root_file:
        if not root_file or root_file.IsZombie():
            print(f"Failed to open ROOT file: {root_file_name}")
            return

        process_subfolder(root_file, folder_structure=False, canvas_func=process_canvas, 
                          output_folder=output_folder, width=width, height=height,
                          nofit=nofit,run_number=run_number)

    # Clean up
    root_file.Close()
   

def process_all_root_files(input_dir:str, output_folder:str, width:int, height:int, nofit:bool):
    # Loop through all root files in the input directory
    for file_name in os.listdir(input_dir):
        if file_name.endswith(".root"):
            root_file_path = os.path.join(input_dir, file_name)
            process_rootfile(root_file_path, output_folder, width, height, nofit)


# Parse command-line arguments
parser = argparse.ArgumentParser(description='''Save ROOT TCanvas as PNG.
    If the root_file is specified, process the root_file. 
    Else if the "all" flag is true, process all the root files in the input_dir.
    Else if the "all" flag is false, process the latest root file in the input_dir.

    Change the style of the plots by modifying the style_list dictionary.
''', formatter_class=argparse.RawTextHelpFormatter)
parser.add_argument('-f', '--root_file', type=str, help='Input ROOT file')
parser.add_argument('-d', '--input_dir', type=str, default='Results', help='Input directory of ROOT file (default: Results). \
                                                            If the input root file is specified, this argument is ignored.')
parser.add_argument('-o', '--output', dest='output_folder', type=str, default='Outputs', help='Output folder (default: Outputs)')
parser.add_argument('-w', '--width', type=int, default=1920, help='Canvas width (default: 1920)')
parser.add_argument('-H', '--height', type=int, default=1080, help='Canvas height (default: 1080)')
parser.add_argument('-A', '--all', action="store_true", help='Process all the root file in the input directory. \
                                           If the input root file is specified, this option is ignored.')
parser.add_argument('--nofit', action="store_true", default=False, help='Do not fit the histograms')
parser.add_argument('-help', action='help', help='Show this help message and exit')

args = parser.parse_args()

if not os.path.exists(args.output_folder):
    os.makedirs(args.output_folder)
    print(f"Directory '{args.output_folder}' created successfully.")
    
# If the root_file is specified, use the root_file.
# Else if the "all" flag is true, process all the root files in the input directory.
# Else if the "all" flag is false, use latest root file in the input directory.
if args.root_file:
   process_rootfile(args.root_file, args.output_folder, args.width, args.height, args.nofit)
else:
    if args.all:
       process_all_root_files(args.input_dir, args.output_folder, args.width, args.height, args.nofit) 
    else:
    # Search for the latest ROOT file in the input dir based on creation date
        root_files = [f for f in os.listdir(args.input_dir) if f.endswith(".root")]
        root_files.sort(key=lambda x: os.path.getctime(os.path.join(args.input_dir, x)), reverse=True)
        if root_files:
            args.root_file = os.path.join(args.input_dir, root_files[0])
        else:
            print(f"No ROOT file found in the '{results_folder}' folder.")
            exit()
        process_rootfile(args.root_file, args.output_folder, args.width, args.height, args.nofit)
