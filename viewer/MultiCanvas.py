from rutil import TCachingFile, Frame, canvas, nostat, keep
import os, sys
import ROOT
from collections import OrderedDict
import random
import time
import threading

def poissonf(x, par):
   return par[0]*TMath.Poisson(x[0],par[1])


      

class MTab:
   counter = 0

   """ container for a list of views in a tab """
   def __init__(self, name, root_tab, tag=""):
      self.name = name
      self.views = []
      self.root_tab = root_tab
      self.i = MTab.counter
      MTab.counter += 1
      self.ecanvas = None
      self.currentView = None
      self.dispatchers = []
      self.tag = tag  # to bin included in filenames for printing

   def addView(self, viewname, nx, ny, drawers=[]):
      view = View(viewname, nx, ny, drawers)
      view.mtab =  self
      self.views.append( view )
      return view

   def printButtonHandler(self):
      if self.currentView is None:
         print("nothing to print")
         return
      filename = "%s%s-%s.png"%(self.name, self.tag, self.currentView.name)
      filename.replace(" ","_")
      print("printing ",self.currentView.name, filename)
      c = self.ecanvas.GetCanvas().Print(filename)
      
   def printToClipboardButtonHandler(self):
      if self.currentView is None:
         print("nothing to print")
         return
      filename = "/tmp/%s%s-%s.png"%(self.name, self.tag, self.currentView.name)
      filename = filename.replace(" ","_")
      print("printing ",self.currentView.name, filename)
      c = self.ecanvas.GetCanvas().Print(filename)
      os.system('xclip -selection clipboard -t image/png "%s"'%filename)
      print("image now in clipboard, past with Ctrl-V")

   def buttonHandler(self, arg):
      self.currentView = arg
      arg.draw()


   def configureButtons(self):
      """ create and configure the buttos for a canvas,  move this into MTab"""
      # extra buttons at the bottom
      printButton = ROOT.TGTextButton(self.buttonFrame,"Print")
      m =  ROOT.TPyDispatcher( self.printButtonHandler )
      self.dispatchers.append(m)
      printButton.Connect("Clicked()", "TPyDispatcher", m, 'Dispatch()' )
      self.buttonFrame.AddFrame(printButton, ROOT.TGLayoutHints(ROOT.kLHintsExpandX|ROOT.kLHintsCenterX|ROOT.kLHintsBottom,5,5,3,4))

      printToClipboardButton = ROOT.TGTextButton(self.buttonFrame,"Copy")
      m =  ROOT.TPyDispatcher( self.printToClipboardButtonHandler )
      self.dispatchers.append(m)
      printToClipboardButton.Connect("Clicked()", "TPyDispatcher", m, 'Dispatch()' )
      self.buttonFrame.AddFrame(printToClipboardButton, ROOT.TGLayoutHints(ROOT.kLHintsExpandX|ROOT.kLHintsCenterX|ROOT.kLHintsBottom,5,5,3,4))
      
      #for v in self.views:
      for v in reversed(self.views):
         draw = ROOT.TGTextButton(self.buttonFrame, v.name)
         m =  ROOT.TPyDispatcher( lambda x=v: self.buttonHandler(x))
         self.dispatchers.append(m)  # must keep a reference
         draw.Connect("Clicked()", "TPyDispatcher", m, 'Dispatch()' )
         self.buttonFrame.AddFrame(draw, ROOT.TGLayoutHints(ROOT.kLHintsExpandX|ROOT.kLHintsCenterX|ROOT.kLHintsBottom,5,5,3,4))



class View:
   """ represent a view, for drawoing onto a canvas """                     
   def __init__(self, name, nx, ny, drawers=[]):
      self.name = name
      self.nx = nx
      self.ny = ny
      self.drawers = drawers

   def draw(self, canvas=None):
      canvas = self.mtab.ecanvas.GetCanvas()
      canvas.cd()
      canvas.Clear()
      canvas.Divide(self.nx, self.ny)
      for i,d in enumerate(self.drawers):
         if d is not None:
            canvas.cd(i+1)
            d.do()
      canvas.Update()


class MainFrame(ROOT.TGMainFrame):
   """version of TGMainFrame with customized ClosedWindow() method
   avoiding annoing crashes """
   
   def __init__(self, *args, **kwargs):
      super().__init__(*args, **kwargs)
      self.closing = False
      
   def CloseWindow(self):
      """ override, let the user decide what to do """
      self.closing = True


class MultiCanvas:
   """
   2013-02-26  modified for batch operation, gui classes crash when
   running without display (e.g. in t3 batch queue)
   in batch mode it becomes a simple list of canvases
   MultiCanvas no longer inherits from TGMainFrame,
   but has an attribute mainFrame
   """

   FileClass = TCachingFile

   def __init__( self, width, height, tag = "", FileClass = None, parent=None, batch=False, title="" ):

      if FileClass is not None:
         MultiCanvas.FileClass = FileClass
      self.title = title
      self.batch=batch
      if tag != "":
         self.tag = "-"+tag
      else:
         self.tag = ""
      
      if width>1:
         # absolut size in pixels
         self.sizex=int(width)
         self.sizey=int(height)
      elif 0<width<=1 and not self.batch:
         # relative size
         self.sizex=int(width*ROOT.gClient.GetRoot().GetWidth())
         self.sizey=int(height*ROOT.gClient.GetRoot().GetHeight())
      else:
         print("fraction width in batch mode? Revert to default")
         self.sizex=800
         self.sizey=600
         

      self.buttonFrames = []
      self.keeper=[]
      self.mtabs = OrderedDict()
      self.first_tab = None

      if self.batch:
         
         ROOT.gROOT.SetBatch(True)
         self.canvas=[]
         
      else:
         self.tabs=[]
         if parent is None:
            parent = ROOT.TGClient.Instance().GetRoot()
         self.mainFrame = MainFrame( parent, self.sizex, self.sizey)
         self.fTab=ROOT.TGTab( self.mainFrame )

         self.mainFrame.AddFrame(self.fTab, ROOT.TGLayoutHints(ROOT.kLHintsExpandX| ROOT.kLHintsExpandY))
         self.fTab.SetLayoutManager( ROOT.TGTabLayout(self.fTab) )
         self.tabLayouter = ROOT.TGTabLayout(self.fTab)
         self.fTab.SetLayoutManager( self.tabLayouter )

         self.mainFrame.SetWindowName( self.title )
         self.mainFrame.MapSubwindows()
         self.mainFrame.Resize(self.mainFrame.GetDefaultSize())
         self.mainFrame.MapWindow()

         
         
   def draw(self, wait=True):
      if not self.batch:
         for i, mtab in enumerate(self.mtabs.values()):
            self.fTab.SetTab(i)
            mtab.configureButtons()  

         if self.first_tab is not None and len(self.first_tab.views)>0:
            self.fTab.SetTab(0)
            self.first_tab.buttonHandler(self.first_tab.views[0])

         self.mainFrame.MapSubwindows()
         self.mainFrame.Resize( self.mainFrame.GetDefaultSize() )
         self.mainFrame.MapWindow()

         if wait:
            # end either by closing the window or hitting return
            t = threading.Thread(target=lambda : input("press return to exit"), daemon=True)
            t.start()
            while not self.mainFrame.closing and t.is_alive():
               time.sleep(0.2)

            
   def addTab(self, tabname):
      """ add a tab with a canvas and a buttonframe
      the canvas is added to the list of canvasses and will be drawn on by the "Views"
      """
      

      if self.batch:
         c = ROOT.TCanvas("c%d"%i, tabname, 10, 10, self.sizex, self.sizey)
         self.canvas.append(c)
      else:
         root_tab = self.fTab.AddTab(tabname          )
         self.tabs.append(root_tab)  # needed?
         mtab = MTab(tabname, root_tab, self.tag)
         self.fCurrentTab = mtab
         self.mtabs[tabname] = mtab
         if self.first_tab is None:
              self.first_tab = mtab

         fCurrentFrame = self.fTab.GetTabContainer(mtab.i)
         fCurrentFrame.SetLayoutManager( ROOT.TGHorizontalLayout(fCurrentFrame) )

         # create the embedded canvas for this tab
         ecanvas = ROOT.TRootEmbeddedCanvas("Ecanvas",fCurrentFrame,self.sizex, self.sizey)
         mtab.ecanvas = ecanvas
         fCurrentFrame.AddFrame(ecanvas, ROOT.TGLayoutHints(ROOT.kLHintsRight| ROOT.kLHintsTop,10,10,10,1))
         # now the button frame
         buttonFrame = ROOT.TGVerticalFrame(fCurrentFrame,40,self.sizey)
         self.buttonFrames.append( buttonFrame )  # FIXME
         mtab.buttonFrame = buttonFrame
         fCurrentFrame.AddFrame(buttonFrame, ROOT.TGLayoutHints(ROOT.kLHintsCenterX, 1 ,1,1,1));

         c = ecanvas.GetCanvas()
         
      return



   def show(self):
      """  """
      if self.batch:
         pass # for now
      sys.exit(1)

      fTabFrame = self.fTab.GetTabContainer(i)  # a TGCompositeFrame
      fCurrentFrame = ROOT.TGHorizontalFrame(fTabFrame, 800, 800)
      fTabFrame.AddFrame(fCurrentFrame,ROOT.TGLayoutHints(ROOT.kLHintsExpandX| ROOT.kLHintsExpandY))
      view.draw()
 

   def add_old(self, viewname, nx, ny, *drawers ):
       # frames are tuples: (function, arguments) to be executed
       c = self.fCurrentTab.addView(viewname, nx, ny, drawers)


   def add(self, viewname, nx=None, ny=None, *drawers ):
      if nx == None and  ny == None and len(drawers) == 0:
         # simplified invocation,, only one string argument for name and histogram id
         view = self.fCurrentTab.addView(viewname, 1, 1, (D1(viewname),) )
      else:
         c = self.fCurrentTab.addView(viewname, nx, ny, drawers)

         
   def __del__(self):
      if not self.batch:
         try:
            self.mainFrame.Cleanup()
         except AttributeError:
            pass


   def printTab(self, tabname, filename=None):
      if filename is None:
         filename = tabname + ".png"

      i = self.tabnames.index( tabname )
      if i>=0 :
         c = self.cd(i)
         c.Draw()
         c.Update()
         c.Print( filename )


   def printAll(self, extension):
      for i in range(len(self.tabnames)):
         c=self.cd(i)
         c.Draw()
         c.Update()
         c.Print("%s%s"%(self.tabnames[i], extension))

   def printAllSinglePdf(self, filename):
      for i in range(len(self.tabnames)):

         c=self.cd(i)
         c.Draw()
         c.Update()
         if i==0:
            c.Print("%s.pdf("%filename)
         elif i<len(self.tabnames)-1:
            c.Print("%s.pdf"%filename)
         else:
            c.Print("%s.pdf)"%filename)



   def cd(self, tabnr):
      if self.batch:
         c = self.canvas[tabnr]
         
      else:
         c = self.fECanvas[tabnr].GetCanvas()

      c.cd()
      return c


   def __getitem__(self, tabname):
      try:
         i = self.tabnames.index( tabname )
         c = self.cd(i)
         return c
      except ValueError:
         print("tab not found :",tabname)
         return None
      
class H:
   def __init__(self, h):
      self.h = h
   def do(self):
      self.h.Draw()


class Drawer:
    def __init__(self, *a, **b):
        self.a = a
        self.b = b

    def do(self):
        self.draw(*self.a, **self.b)

    def draw(self, hid,
             xtitle=None, xmin=0., xmax=-1.,
             ytitle="", ymin=0, ymax=-1, option=""):
       """ overwrite this method, it is only an example !!!!! """
       if xtitle is None:
           xtitle = hid
       if option == "":
           try:
               option = tf.option
           except AttributeError:
               option = ""
       F = Frame("",xtitle, xmin, xmax, ytitle, ymin, ymax)
       for tf in MultiCanvas.FileClass.files:
           h = tf.Get(hid)
           F.add(tf.Get(hid), option, label=tf.label)
       F.drawLegend()
       keep(F)

       
    def drawLegend(self, F, legend, xl, xh, yl, yh, title):
       if legend is None:
          F.Draw()
       else:
          height = 0.1 + 0.1 * len(self.histos)
          width = 0.26
          if legend == "NE":
              xl, xh = 0.96 - width, 0.96
              yl, yh = 0.95 - height, 0.95
          elif legend == "NW":
              xl, xh = 0.15, 0.15 + width
              yl, yh = 0.95 - height, 0.95
          elif legend == "E":
              xl, xh = 0.96-width, 0.96,
              yl, yh = 0.5-height/2., 0.5+height/2.
          elif legend == "N":
              xl, xh = 0.5 - 0.5 * width, 0.5 + 0.5 * width
              yl, yh = 0.95 - height, 0.95
          elif legend == "S":
              xl, xh = 0.5 - 0.5 * width, 0.5 + 0.5 * width
              yl, yh = 0.15, 0.15 + height
          elif legend == "SE":
              xl, xh = 0.96 - width, 0.96,
              yl, yh = 0.15, 0.15 + height
          elif legend == "SW":
              xl, xh = 0.15, 0.15 + width
              yl, yh = 0.15, 0.15 + height
              
          F.drawLegend(xl=xl, xh=xh, yl=yl, yh=yh, title = title)

    def get_xrange(self, histogram ,xmin=0, xmax=-1.):
       if xmin >= xmax:
          # choose x-range from histogram
          try:
             axis = histogram.GetXaxis()
             xmin, xmax = axis.GetXmin(), axis.GetXmax()
             if xmax <= xmin :
                print("invalid x range ",xmin, xmax)
          except:
             print("empty histogram list, unable to determine range")
             xmin, xmax = 0, 1.
       return xmin, xmax
    
    def get_yrange(self, histogram ,ymin=0, ymax=-1.):
       if ymin >= ymax:
          # choose x-range from histogram
          try:
             axis = histogram.GetYaxis()
             ymin, ymax = axis.GetXmin(), axis.GetXmax()
             if ymax <= ymin :
                print("invalid y range ",ymin, ymax)
          except:
             print("empty histogram list, unable to determine range")
             ymin, ymax = 0, 1.
       return ymin, ymax

class DX(Drawer):
  """ direct Drawer """
  def draw(self, hr, xtitle="", xmin=0, xmax=-1., ytitle="", ymin=0, ymax=-1.,option="", title=""):
     h = hr.Clone()
     xmin, xmax = self.get_xrange(h)
     if type(h) in (ROOT.TH2F,):
        ymin, ymax = self.get_yrange(h)
     elif type(h) in (ROOT.TH1F,):
        ymin, ymax  = 0, h.GetMaximum() * 1.05
     F = Frame(title, xtitle, xmin, xmax, ytitle, ymin, ymax)
     F.add(h, option)
     F.Draw()
     keep(h, F)


class D0(Drawer):
  def draw(self,hid):
    for tf in MultiCanvas.FileClass.files:
      hr = tf.Get(hid)
      try:
         h = hr.Clone()
         h.Draw()
         keep(h)
      except AttributeError:
        print("error getting ",hid," ->",hr)



class D1(Drawer):
   def draw(self,hid, xtitle=None, xmin=0., xmax=-1,
            ytitle="", ymin=None, ymax=-1, 
            option="", norm=None, rebin=None, logy=False, logz=False,
            legend = "",
            xl=0.6, xh=0.96, yl=0.8, yh=0.85,
            title=None, fit=None):

       if xtitle is None:
           xtitle = hid

       if xmin >= xmax:
          # choose x-range from histogram
          try:
             axis = MultiCanvas.FileClass.files[0].Get(hid).GetXaxis()
             xmin, xmax = axis.GetXmin(), axis.GetXmax()
             if xmax <= xmin :
                print("D1.draw : invalid x range ",xmin, xmax)
          except:
             print("D1.draw : empty histogram list, unable to determine range")
             xmin, xmax = 0, 1.

       F = Frame("",xtitle, xmin, xmax, ytitle, ymin, ymax, logy=logy, logz=logz)

       self.histos = []
       for tf in MultiCanvas.FileClass.files:
          try:
             h = tf.Get(hid).Clone()
             #print "MultiCanvas.D1.draw norm=",norm
             if norm is not None and h.GetDimension() != 2:
                if callable(norm):
                  h.Scale(norm(tf, hid))
                else:
                  h.Scale(norm)
             if rebin is not None:
                h.Rebin(rebin)
             if option == "*":
                doption = tf.option
             elif option == "" and (h.GetDimension() == 2):
                doption = "colz"
             elif option == "" and type(h) == ROOT.TProfile:
                doption = "P"
             elif option == "" and type(h) in (ROOT.TH1F, ROOT.TH1D):
                doption = "H"
             else:
                doption = option
             F.add(h, doption, width=2, color=tf.color, label=tf.label)
             self.histos.append(h)
          except AttributeError:
             #print("D1.draw : something went wrong")
             pass

       if title is None:
          title = hid

       if legend is None:
          F.Draw()
       else:
          height = 0.1 + 0.1 * len(self.histos)
          width = 0.26
          if legend == "NE":
              xl, xh = 0.96 - width, 0.96
              yl, yh = 0.95 - height, 0.95
          elif legend == "NW":
              xl, xh = 0.15, 0.15 + width
              yl, yh = 0.95 - height, 0.95
          elif legend == "E":
              xl, xh = 0.96-width, 0.96,
              yl, yh = 0.5-height/2., 0.5+height/2.
          elif legend == "N":
              xl, xh = 0.5 - 0.5 * width, 0.5 + 0.5 * width
              yl, yh = 0.95 - height, 0.95
          elif legend == "S":
              xl, xh = 0.5 - 0.5 * width, 0.5 + 0.5 * width
              yl, yh = 0.15, 0.15 + height
          elif legend == "SE":
              xl, xh = 0.96 - width, 0.96,
              yl, yh = 0.15, 0.15 + height
          elif legend == "SW":
              xl, xh = 0.15, 0.15 + width
              yl, yh = 0.15, 0.15 + height
            
          F.drawLegend(xl=xl, xh=xh, yl=yl, yh=yh, title = title)
       
       if fit is not None:
          for h in self.histos:
             if fit in ("gaus","pol0","pol1","pol2","expo"):
                h.Fit( fit ,"", "Same")

       keep(F)


class D2(Drawer):
   """ drawer for the ratio of two histograms """
   def draw(self, hid1, hid2, xtitle=None, xmin=0., xmax=-1,
            ytitle="", ymin=0, ymax=-1, 
            option="", norm=None, rebin=None, logy=False,
            legend = "",
            xl=0.6, xh=0.96, yl=0.8, yh=0.85, title=None, fit=None):

       if xtitle is None:
           xtitle = hid1 + "/" + hid2

       if xmin >= xmax:
          # choose x-range from histogram
          try:
             axis = MultiCanvas.FileClass.files[0].Get(hid1).GetXaxis()
             xmin, xmax = axis.GetXmin(), axis.GetXmax()
             if xmax <= xmin :
                print("D2.draw : invalid x range ",xmin, xmax)
          except:
             print("D2.draw : empty histogram list, unable to determine range")
             xmin, xmax = 0, 1.

       F = Frame("",xtitle, xmin, xmax, ytitle, ymin, ymax, logy=logy)

       self.histos = []
       for tf in MultiCanvas.FileClass.files:
          if True:#try:
             h1 = tf.Get(hid1).Clone(str(random.randint(1000000, 99999999)))
             if norm: h1.Scale(norm(tf, hid1))
             h2 = tf.Get(hid2).Clone(str(random.randint(1000000, 99999999)))
             if norm: h2.Scale(norm(tf, hid2))
             nbin = h1.GetNbinsX()
             h = ROOT.TH1F(hid1+hid2+"_%d"%tf.id, "", nbin, h1.GetXaxis().GetXmin(), h1.GetXaxis().GetXmax())
             # the Divide function of ROOT seems to yield empty histograms
             for b in range(1,h.GetNbinsX()+1):
                if h2.GetBinContent(b) > 0:
                   v =  h1.GetBinContent(b)/float(h2.GetBinContent(b))
                   h.SetBinContent(b, v)
                   h.SetBinError(b, 0)
                else:
                   h.SetBinContent(b, 0)
                   h.SetBinError(b, 0)
             #if norm is not None: 
             #   h.Scale(norm(tf, h))
             if rebin is not None:
                h.Rebin(rebin)
             if option == "":
                 try:
                    option = tf.option
                 except AttributeError:
                    option ="hist"
             F.add(h, option, label=tf.label)
             self.histos.append(h)
          else:#except:
             pass

       if title is None:
          title = hid1 + "/" + hid2

       #F.drawLegend(xl=xl, xh=xh, yl=yl, yh=yh, title = title)
       self.drawLegend(F, legend = legend, xl=xl, xh=xh, yl=yl, yh=yh, title = title)
       
       if fit is not None:
          for h in self.histos:
             if fit in ("gaus","pol0","pol1","pol2","expo"):
                h.Fit( fit ,"", "Same")

       keep(F)


class D4(Drawer):
   def draw(self,hid, xtitle=None, xmin=0., xmax=1., ytitle="", ymin=0, ymax=-1, option=""):
       if xtitle is None:
           xtitle = hid
       F = Frame("",xtitle, xmin, xmax, ytitle, ymin, ymax)
       for tf in MultiCanvas.FileClass.files:
           h = tf.Get(hid).Clone()
           if h.GetEntries() > 0:
              h.Scale(1./h.GetEntries())
           F.add(h, option)
       F.drawLegend()
       keep(F,h)

class D3(Drawer):
   def draw(self,hid, hidref, xtitle=None, xmin=0., xmax=1., ytitle="", ymin=0, ymax=-1, option=""):
       if xtitle is None:
           xtitle = hid
       F = Frame("",xtitle, xmin, xmax, ytitle, ymin, ymax)
       for tf in MultiCanvas.FileClass.files:
           h = tf.Get(hid).Clone()
           href = tf.Get(hid)
           h.Divide(href)
           F.add(h, option)
       F.drawLegend()
       keep(F,h)


class DP(Drawer):
   def draw(self,hid, xtitle=None, xmin=0., xmax=-1, ytitle="", ymin=0, ymax=-1, 
            option="", norm=None, rebin=None, logy=False, logz=False, 
            xl=0.6, xh=0.96, yl=0.8, yh=0.85, title=None, fit=None, projection=1):

       if xtitle is None:
           xtitle = hid

       if xmin >= xmax:
          # choose x-range from histogram
          try:
             if projection == 1:
               axis = MultiCanvas.FileClass.files[0].Get(hid).GetXaxis()
             else:
               axis = MultiCanvas.FileClass.files[0].Get(hid).GetYaxis()
             xmin, xmax = axis.GetXmin(), axis.GetXmax()
             if xmax <= xmin :
                print("D1.draw : invalid x range ",xmin, xmax)
          except:
             print("D1.draw : empty histogram list, unable to determine range")
             xmin, xmax = 0, 1.

       F = Frame("",xtitle, xmin, xmax, ytitle, ymin, ymax, logy=logy)

       self.histos = []
       for tf in MultiCanvas.FileClass.files:
          try:
             if projection == 1:
                 h = tf.Get(hid).ProfileX().Clone()
             elif projection == 2:
                 h = tf.Get(hid).ProfileY().Clone()
             else:
                 continue

             if norm is not None:
                h.Scale(norm(tf, hid))
             if rebin is not None:
                h.Rebin(rebin)

             if option == "smallfullcircle" and tf.label.find("2017")>=0:
                F.add(h, "smallopencircle", label=tf.label)
             else:
                F.add(h, option, label=tf.label)

             self.histos.append(h)
          except IndexError:
             print("fubar")
             pass

       if title is None:
          title = hid

       F.drawLegend(xl=xl, xh=xh, yl=yl, yh=yh, title = title)
       
       if fit is not None:
          for h in self.histos:
             if fit in ("gaus","pol0","pol1","pol2"):
                h.Fit( fit ,"", "Same")

       keep(F)

  

def entries(rootfile, hid=None):
   if hid is not None:
      I = rootfile.Get(hid).GetEntries()
   elif h is not None:
      I = h.GetEntries()
   if I>0:
      return 1./I
   else:
      return 1

def integral(rootfile, hid=None, h=None):
   if hid is not None:
      I = rootfile.Get(hid).Integral()
   elif h is not None:
      I = h.Integral()
   if I>0:
      return 1./I
   else:
      return 1


def sideband(rootfile, hid=None):
    h = rootfile.Get(hid)
    nb = h.GetNbinsX()
    I1 = h.Integral(1, int(0.3*nb))
    I2 = h.Integral(int(0.7*nb),nb)
    return 1./(I1+I2)

