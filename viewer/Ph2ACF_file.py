import os, re
from ROOT import TFile, TH2F, TH1F
from rutil import tgraph_from_list, keep, randid

class Ph2ACF_file:
    files = []
    def __init__(self, path, color=1, label=None, load_scurves=False, caching=True, defaultpath=""):
        if not os.path.exists(path):
            if not os.path.exists( os.path.join(defaultpath, path) ):
                print("P2ACF_file  not found ",path)
            path = os.path.join(defaultpath, path)

        self.path=path
        self.tfile=TFile(path)
        self.files.append(self)
        self.color=color
        self.label=label
        self.load_scurves = load_scurves # slow !
        self.cache={}
        self.cwd=""
        self.caching = caching
        # get the histograms
        self.histos = {}
        self.scurves = {}
        self.strings = {}
        board=[0,1]
        optical_group=list(range(0, 8))
        hybrid=list(range(0, 8))  # was 0
        # Flag to stop the loop when directory is found
        directory_found = False

        # Iterate through all combinations of board, optical_group, and hybrid
        for b in board:
            if directory_found:  # Stop if directory was already found
                break
            for og in optical_group:
                if directory_found:  # Stop if directory was already found
                    break
                for h in hybrid:
                    # Construct the directory path
                    directory_path = f"Detector/Board_{b}/OpticalGroup_{og}/Hybrid_{h}"
                    
                    # Try to get the corresponding directory from the file
                    top = self.tfile.GetDirectory(directory_path)
                    
                    # Check if the directory exists
                    if top:
                        directory_found = True
                        break  # Break from the innermost loop


        # top = self.tfile.GetDirectory(f"Detector/Board_{board}/OpticalGroup_{optical_group}/Hybrid_{hybrid}")
        chip_folders = []
        all_histos = []
        self.chipid_list = []
        for k in top.GetListOfKeys():
            if k.GetName().startswith("Chip_"):
                chip = k.GetName()
                chipid = int(chip[len("Chip_"):])
                self.chipid_list.append(chipid)
                d = top.GetDirectory(chip)
                for tc in d.GetListOfKeys():
                    tc_name = tc.GetName()
                    try:
                        item = d.Get(tc_name)
                        if item.ClassName() == "TGraph":
                            all_histos.append(item)
                        elif item.ClassName() == "TCanvas":
                            for p in d.Get(tc_name).GetListOfPrimitives():
                                if p.Class_Name() in ("TH1F", "TH2F"):
                                    all_histos.append(p)

                                if self.load_scurves and p.ClassName() == "TH3F":
                                    all_histos.append(p)
                        else:
                            #print("not prepared for finding a ",item.ClassName(), "here.  Name= ", tc_name)
                            pass
                            
                    except AttributeError:
                        print("Ph2ACF_file:init unhandled AttributeError")
                        pass

        for h in all_histos:
            group, hid, chipid = self.split_histo_name(h.GetName())
            if hid not in self.histos.keys():
                self.histos[hid] = {}
            self.histos[hid][chipid] = h

        self.strings = self.get_strings(0)
        return


    def get_strings(self, board):
        result = {}
        d = self.tfile.GetDirectory(f"Detector/Board_{board}")
        for k in d.GetListOfKeys():
            item = d.Get(k.GetName())
            if item.ClassName() == "TObjString":
                result[str(k.GetName())] = str(item)
        return result


    def split_histo_name(self, name):
        # name looks like   "or D_B(0)_O(0)_H(0)_LatencyScan_Chip(15)"
        #  or "D_B(0)_O(0)_H(0)_DQM_VINA_Chip(15)"
        m = re.match("D_B\((\d*)\)_O\((\d*)\)_H\((\d*)\)_(.*)_Chip\((\d\d?)\)", name)
        if m is not None:
            chipid = m.group(5)
            group = f"D_B({m.group(1)})_O({m.group(2)})_H({m.group(2)})"
            hid = m.group(4)
            return group, hid, chipid
        else:
            print("Ph2ACF_file::split_histo_name failed for name ",name)
            return None, None, None
        
    def Get(self, hid_and_chip, norm=None, clone=True, rebin=None):
        """ generic get histogram """
        hid,chipid = hid_and_chip.split(":")
        
        if chipid == "all":
            return self.Get_module_map(hid)

        m = re.match("SCurve_(\d*)_(\d*)", hid)
        if m is not None:
            col = int(m.group(1))
            row = int(m.group(2))
            return self.GetScurveHist(chipid, col, row)
        
        try:
            return self.histos[hid][chipid]
        except KeyError:
            return None

        
    def Get_module_map(self, hid):
        """
        synthesize a quad module map from the CROC histograms
        chip positions:    
                    -------------
                   _|  0      1 |
        connector |             |
                  |_            |
                    |  3      2 |
                    -------------
        chip id according to self.chipid_list[position]
        TEPX  : (15,14,13,12)
        TPBX  : (0,1,2,3)
        """
        key = f"{hid}:quad",f"{hid}"
        if key in self.cache:
            return self.cache[key]
        
        ncol, nrow = 432, 336
        hmap = TH2F(f"{hid}:quad",f"{hid}", ncol*2, 0, ncol*2, nrow*2, 0, nrow*2);
        if min(self.chipid_list) > 11: 
            chipid_map = (15,14,13,12) # TEPX
        else:
            chipid_map = (0,1,2,3) # TBPX
            
        for chip_position, chip_id in enumerate(chipid_map):
            if not chip_id in self.chipid_list:
                continue
            try:
                h = self.histos[hid][f"{chip_id}"]
                for col in range(ncol):
                    for row in range(nrow):
                        value = h.GetBinContent(col+1, row+1)
                        if chip_position == 3:
                            mcol = col
                            mrow = nrow-row
                        elif chip_position == 2:
                            mcol = col + ncol
                            mrow = nrow-row
                        elif chip_position == 1:
                            mcol = 2 * ncol - col -1
                            mrow = nrow + row - 1
                        elif chip_position == 0:
                            mcol = ncol - col -1
                            mrow = nrow + row -1
                        hmap.SetBinContent(mcol+1, mrow+1, value)
            except KeyError:
                # missing chip
                pass
        self.cache[key] = hmap
        return hmap
                                       
                                       

    def GetScurve(self, chip, col, row):
        """ retrieve an S curve as a tgraph """
        try:
            SCurveMap = self.histos["SCurveMap"][f"{chip}"]
            values = [SCurveMap.GetBinContent(col+1, row+1, j+1) for j in range(SCurveMap.GetNbinsZ())]
            tg = tgraph_from_list(values)
            tg.SetTitle(f"{chip} row {row} col {col}")
            keep(tg)
            return tg
        except KeyError:
            print("scurve not found ",chip, col, row)
            return None

        
    def GetScurveHist(self, chip, col, row):
        """ retrieve an S curve as a histogram """
        try:
            SCurveMap = self.histos["SCurveMap"][f"{chip}"]
            values = [SCurveMap.GetBinContent(col+1, row+1, j+1) for j in range(SCurveMap.GetNbinsZ())]
            nbin = len(values)
            th1 = TH1F(randid(), f"{chip} row {row} col {col}", nbin, 0, nbin)
            for bin in range(nbin):
                th1.SetBinContent(bin+1, values[bin])
            keep(th1)
            return th1
        except KeyError:
            print("scurve not found ",chip, col, row)
            return None
        
    
