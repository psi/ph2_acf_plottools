## Ph2_ACF file viewer


This is a tool/toolkit for viewing the content of root files created by Ph2_ACF more efficiently than with the root browser.

A working example application, called viewer.py, is part of the repository. It should be copied to the working directory and adapted as needed. 


ROOT and the other files from this repository should be in the PYTHONPATH.
```
MultiCanvas.py
Ph2_ACF_file.py
rutil.py
````
The key functionality, provided by MultiCanvas.py, is to provide quick access to panels displaying multiple histograms through Buttons and Tabs. 
Ph2_ACF_file.py interfaces the the root files of Ph2ACF to MultiCanvas (originally developed for more generic root files). 

The viewer application will often create only a single tab. Pressing one of the buttons in the button bar on the left hand side of the window displays the corresponding 
panel consisting of one or more histograms.
In addition to the panel-buttons there are buttons for creating images of the currently displayed panel, either as a .png file in the clipboard.

 

## the example viewer:
In the following it is assumed that viewer.py resides in a directory that has a subdirectory called Results, containing the root files.

The viewer application is invoked by
```
python3 viewer.py <root-file>
```

A root file can be specified by either the physical path or the run number. This application essentially groups histograms from a common directory into a tab.
For each histogram name a panel of four histograms for the four CROCs is created, accessible with a button in the button bar on the left hand side of the panel.
Obviously the typical file will contain only a single tab.


Multiple rootfiles can be given,  the corresponding histograms are superimposed with different colors. A label for the legend of the plots can by appended with an '@' sign,
example:
```
python3 viewer.py 158@before 161@after
```




### customizing the viewer
The example is useful as it is for a quick display of the result of an aribtrary Ph2_ACF calibration run. 
For more extensive plotting, pre-processing, fitting, etc, one can adapt the viewer.


A simple viewer application consists of two steps
1) parsing command line arguments and opening the root files
2) configuring the MultiCanvas


Optionally, some processing can be done between 1) and 2).


The MultiCanvas is instantiated by a line like
````
w = MultiCanvas(1000, 800, FileClass=Ph2ACF_file, title="")
````
The first two arguments are the window size in pixels.
Subsequently tabs can be added by
```
w.addTab(<tab-name>)
```
and panels are added to the most recently created tab by
```
w.add(<panel-name>, nx, ny, <Drawer-1>, <Drawer-2>, ...)
````
where 
<panel-name> is the name that will appear on the button
nx,ny are the number rows and columns of histogram on the panel
<Drawer-n> is one of the Drawer objects defined in MultiCanvas.py (or a derived, customized class), there can be up to nx*ny Drawers.

Finally contol is passed to the gui by calling the draw() method of the MultiCanvas.


A drawer must be capable of displaying a histogram (or overlaying multiple histograms) on demand. Two Drawers are used in the example
```
DX(<TH** or TGraph>, **kwargs)
D1("label", **kwargs)
```

The former accepts a root object with a Draw() method, e.g. histograms and TGraphs, while the latter accepts a lable and retrieves the corresponding histogram from the Ph2ACF_file(s) specified as command line argument.



The remaining keyword arguments can be
```
xtitle=
xmin=
xmax=
ytitle=
ymin=
ymax=
logz=True/False
option="P"/"h"/"colz"
```

