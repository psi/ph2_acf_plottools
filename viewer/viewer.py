#!/usr/bin/env python3

import os
import re
import sys
import argparse
import glob
from ROOT import TMath, TF1, THStack, TFile, TH1F
from rutil import keep, tgraph_from_list
from MultiCanvas import MultiCanvas, D1, DX
from Ph2ACF_file import Ph2ACF_file



def add2x2(w, key, **kwargs):
    """helper for making a panel with one plot per croc"""
    #if len(chipid_list) < 4:
    #    return
    if min(chipid_list) > 11: 
        chipid_map = (15,14,13,12) # TEPX
    else:
        chipid_map = (0,1,2,3) # TBPX
        
    view = [None,None,None,None]
    for position,chipid in enumerate(chipid_map):
        if chipid in chipid_list:
            view[position] = D1(f"{key}:{chipid}", **kwargs)
    w.add(key,  2,  2, *view)


def select_scurves(ph2acf_file, chip_sel):
    if chip_sel is None:
        chips = chipid_list
    else:
        chips = (chip_sel,)

    scurves = []
    for chip in chips:
        SCurveMap = ph2acf_file.Get(f"SCurveMap:{chip}")
        if SCurveMap is None:
            continue
        for col in range(SCurveMap.GetNbinsX()):
            for row in range(SCurveMap.GetNbinsY()):
                values = [
                    SCurveMap.GetBinContent(col + 1, row + 1, j + 1)
                    for j in range(SCurveMap.GetNbinsZ())
                ]
                if min(values) < 0.1 and max(values) > 0.9 and max(values) <= 1:
                    continue
                if max(values) == 0:
                    continue

                tg = tgraph_from_list(values)
                tg.SetTitle(f"{chip} row {row} col {col}")
                tg.SetMarkerStyle(20)
                keep(tg)

                if chip_sel is None:
                    scurves.append((chip, col, row, tg))
                else:
                    scurves.append((col, row, tg))

    return scurves


def diffmap(h1, h2, hdiff2D, hdiff1D=None, h11d=None, h21d=None):
    """ helper for creating difference histograms between two files from 2d maps  """
    for col in range(h1.GetNbinsX()):
        for row in range(h1.GetNbinsY()):
            v1 = h1.GetBinContent(col+1, row+1)
            v2 = h2.GetBinContent(col+1, row+1)
            hdiff2D.SetBinContent(col+1, row+1, v2-v1)
            if hdiff1D is not None:
                hdiff1D.Fill(v2-v1)
            if h11d is not None:
                h11d.Fill(v1)
            if h21d is not None:
                h21d.Fill(v2)

                
def palette(c):
    color_list = (1, 2, 4, 8, 807, 42, 46, 38, 31, 9)
    if c < len(color_list):
        return color_list[c]
    else:
        return c


parser = argparse.ArgumentParser(description="display Ph2ACF histograms for IT quads")
parser.add_argument(
    "file",
    help="run number or Ph2ACF root file (optionally followed by @label)",
    nargs="+",
    default="",
)
parser.add_argument(
    "-e", "--errors", action="store_true",
    help="print error counters from the root file"
)
parser.add_argument(
    "-s", "--scurves", action="store_true", help="load scurves from SCurveMap (slow)"
)
parser.add_argument(
    "-d", "--diff", action="store_true", help="show differences"
)
parser.add_argument("-v", "--verbose", action="store_true", help="be more verbose")
args = parser.parse_args()
show_tgraph_of_first_file_only = False

calibrations = []


color = 0
for f in args.file:
    try:
        path, label = f.split("@")
    except ValueError:
        path, label = f, ""

    if path == "-":
        # the last one
        globpath = "Results/Run??????_*.root"
        try:
            path = sorted(glob.glob(globpath))[-1]
            runnumber = int(path[len("Results/Run") : len("Results/Run") + 6])
            print("run ", runnumber)
        except IndexError:
            print("no runs found")
            sys.exit(1)
    try:
        runnumber = int(path)
        globpath = f"Results/Run{runnumber:06}_*.root"
        for path in glob.glob(globpath):
            break
    except ValueError:
        pass

    if os.path.exists(path):
        if args.verbose:
            print("opening ", path)
        m = re.match(".*Run(\d\d\d\d\d\d)_(\w*).root", path)
        if m is not None:
            run = int(m.group(1))
            calib = m.group(2)
        else:
            m = re.match("MonitorResults/MonitorDQM_(.*).root", path)
            if m is not None:
                calib = "Monitoring"
            else:
                print(
                    "unknown filename pattern, unable to identify type of calibration"
                )
                calib = None

        tf = Ph2ACF_file(
            path, label=label, color=palette(color), load_scurves=args.scurves
        )
        color += 1
        tf.calibration = calib
        if calib not in calibrations and calib is not None:
            calibrations.append(calib)
    else:
        print("file not found ", path)
        sys.exit(1)



################################## main ##############################################


joint_keys = []
calib_keys = {}
chipid_list = None

for f in Ph2ACF_file.files:
    if chipid_list is None:
        chipid_list = f.chipid_list
    elif f.chipid_list != chipid_list:
        print("inconsistent chip id lists ", f.chipid_list, "<>", chipid_list, " !")

    joint_keys += [k for k in f.histos.keys() if k not in joint_keys]
    if f.calibration is not None and f.calibration not in calib_keys.keys():
        calib_keys[f.calibration] = [k for k in f.histos.keys()]

    if args.errors:
        print(f"*** {f.path} ***")
        for name in f.strings:
            print(name, f.strings[name])
        print(f"***")
            
    

if args.verbose:
    print(calib_keys)
    print(joint_keys)


  

w = MultiCanvas(1000, 800, FileClass=Ph2ACF_file, title="")


# diff plots if selected (--diff) and applicable (2 files and scurves so far)
if args.diff and len(Ph2ACF_file.files) !=2 :
    print("diff only works for two files")
elif args.diff:
    if "SCurve" in calib:
        # 2d map differences
        for variable in ("Threshold","Noise","ToT"):
            w.addTab(variable)
            mindiff, maxdiff  = {"Threshold":(-200, 200), "Noise":(-50, 50), "ToT":(-20,20)}[variable]
            for chipid in ["all"] + chipid_list:
                h1t = Ph2ACF_file.files[0].Get(f"{variable}2D:{chipid}").Clone()
                h2t = Ph2ACF_file.files[1].Get(f"{variable}2D:{chipid}").Clone()
                hdiff2D = h1t.Clone(f"diff{variable}")
                hdiff1D =  TH1F(f"diff{variable}1D-{chipid}", "variable", 400, mindiff, maxdiff)
                diffmap(h1t, h2t, hdiff2D, hdiff1D, None, None)
                hdiff2D.SetMinimum(mindiff)
                hdiff2D.SetMaximum(maxdiff)
                w.add(f"{variable}2D-{chipid}", 1, 1, DX(hdiff2D, title=f"{chipid}", option="colz"))
                w.add(f"{variable}1D-{chipid}", 1, 1, DX(hdiff1D, title=f"{chipid}", option="h"))

# non-diff plots                
for calib in calibrations:
    w.addTab(calib)

    if calib == "PixelAlive":
        w.add("module", 1, 1, D1("PixelAlive:all", option="colz"))
        
        for chipid in chipid_list:
            w.add(f"PixelAlive{chipid}", 1, 1, D1(f"PixelAlive:{chipid}", option="colz"))

        for key in calib_keys[calib]:
            add2x2(w, key)

    elif calib == "SCurve":
        add2x2(w, "SCurves", xtitle="#Delta Vcal", ymin=0, ymax=1, logz=True)

        for key in ("Threshold1D", "Noise1D"):
            add2x2(w, key, xtitle="#Delta Vcal")
            
        for key in ("ReadoutErrors", "FitErrors", "Threshold2D", "Noise2D", "ToT2D"):
            add2x2(w, key, xtitle="column", ytitle="row")

        if args.scurves:
            tf = Ph2ACF_file.files[0]  # select based on the first file only
            for chip in chipid_list:
                w.addTab(f"scurves {chip}")
                scurves = select_scurves(tf, chip)
                print("selected ", len(scurves), " curves for chip ", chip)
                ncurve = 0
                button = 0
                drawers = [[]]
                for col, row, tg in scurves:
                    if show_tgraph_of_first_file_only:
                        tg.SetMarkerStyle(20)
                        drawers[button].append(
                            DX(
                                tg,
                                title=f"{chip}_{col}_{row}",
                                xmin=0,
                                xmax=50,
                                ymin=0,
                                ymax=1.1,
                                option="P",
                            )
                        )
                    else:
                        # show histograms for scurves of all files (selection still based on the first one only)
                        drawers[button].append(
                            D1(f"SCurve_{col}_{row}:{chip}", legend="NW")
                        )
                    ncurve += 1
                    if ncurve % (3 * 3) == 0 or ncurve == len(scurves):
                        w.add(f"{button}", 3, 3, *drawers[button])
                        drawers.append([])
                        button += 1
                        if button == 20:
                            print(
                                f"too many scurves ({len(scurves)}, only {ncurve} are shown"
                            )
                            break

    elif calib == "Monitoring":
        add2x2(
            w,
            "DQM_INTERNAL_NTC",
            xtitle="time",
            option="smallfullcircle",
            ymin=-50,
            ymax=100,
            ytitle="T (C)",
        )
        add2x2(
            w,
            "DQM_VINA",
            xtitle="time",
            option="smallfullcircle",
            ymin=0,
            ymax=1,
            ytitle="VINA/4",
        )
        add2x2(
            w,
            "DQM_VIND",
            xtitle="time",
            option="smallfullcircle",
            ymin=0,
            ymax=1,
            ytitle="VIND/4",
        )
        add2x2(
            w,
            "DQM_VDDA",
            xtitle="time",
            option="smallfullcircle",
            ymin=0,
            ymax=1,
            ytitle="VDDA/2",
        )
        add2x2(
            w,
            "DQM_VDDD",
            xtitle="time",
            option="smallfullcircle",
            ymin=0,
            ymax=1,
            ytitle="VDDD/2",
        )
        add2x2(
            w,
            "DQM_ANA_IN_CURR",
            xtitle="time",
            option="smallfullcircle",
            ymin=0,
            ymax=100,
            ytitle="I[?]",
        )
        add2x2(
            w,
            "DQM_DIG_IN_CURR",
            xtitle="time",
            option="smallfullcircle",
            ymin=0,
            ymax=100,
            ytitle="I [?]",
        )
        add2x2(
            w,
            "DQM_Iref",
            xtitle="time",
            option="smallfullcircle",
            ymin=0,
            ymax=10,
            ytitle="Iref [?]",
        )
    else:
        # generic
        for key in calib_keys[calib]:
            if key in ("Occ2D", "ToT2D", "TDAC2D"):
                w.add(f"{key}:module", 1, 1, D1(f"{key}:all", option="colz"))
                
            if key in ("PixelAlive", "ToT2D", "ReadoutErrors", "TDAC2D"):
                add2x2(w, key, xtitle="column", ytitle="row")
            else:
                add2x2(w, key)

w.draw(wait=True)
